package com.atlassian.confluence.plugins.extractor.tutorial;

import com.atlassian.confluence.index.attachment.AttachmentTextExtractor;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.util.io.InputStreamSource;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class SimpleAttachmentTextExtractor implements AttachmentTextExtractor {
    private final AttachmentManager attachmentManager;

    @Autowired
    public SimpleAttachmentTextExtractor(@ComponentImport AttachmentManager attachmentManager) {
        this.attachmentManager = requireNonNull(attachmentManager);
    }

    @Override
    public List<String> getFileExtensions() {
        return Collections.singletonList("java");
    }

    @Override
    public List<String> getMimeTypes() {
        return Collections.singletonList("text/java");
    }

    @Override
    public Optional<InputStreamSource> extract(Attachment attachment) {
        try (InputStream is = attachmentManager.getAttachmentData(attachment)) {
            if (is != null) {
                String text = IOUtils.toString(is, StandardCharsets.UTF_8);
                return Optional.of(() -> IOUtils.toInputStream(text, StandardCharsets.UTF_8));
            }
            return Optional.empty();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
